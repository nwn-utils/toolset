# NWN (BioWare) Data Toolset

This toolset provides data structures and routines for interacting with data files for BioWare RPG games, providing ease of extension for games which do not yet have direct support.

## File structure
 - `nwn/`: The Python module for the game data files and structures
   - `nwn/common/`: Common utilities and classes that all file and data types may use
   - `nwn/data/`: Classes to represent the data that BioWare games store and use
   - `nwn/files/`: The various archive and internal file types that BioWare games use
 - `erf.py`: Application that uses the `nwn` module to read and write ERF (and MOD, RIM, HAK, etc.) archives

## Usage

### With top-level app

	# Extract the "Chapter1E" game module from the NWN installation to /path/to/out
	python3 erf.py extract nwm/Chapter1E.nwm /path/to/out
	# Then use the files in that directory to create a new module
	python3 erf.py create modules/new.mod /path/to/out
	# Then add a new file to the module we just created
	python3 erf.py add modules/new.mod /path/to/new/file


### With Python (Python 3) module

	# Read a NWN SAV file and print the Module Variable count
	import io
	from nwn.files.gff import GFFFile
	from nwn.files.erf import ERFFile, formats
	from nwn.common.util import eprint

	file_ifo = None
	with ERFFile('sample.sav', 'r') as erf_in:
		def add_file(f):
			f.data = io.BytesIO()
			f.copy_data(erf_in, f.data)
			f.data.seek(0)
			global file_ifo # Please don't actually do this in non-testing code
			file_ifo = f

		erf_in.read(None
			, lambda f, *p: (f.name == 'Module' and f.type == formats.Formats['ifo'].type)
			, add_file)
		archive_type = erf_in.file_type
		version = erf_in.version

	if file_ifo is None:
		eprint('Unable to locate Module.ifo')
	else:
		gff = GFFFile.from_file(file_ifo.data)
		field_vars = next(f for f in gff.root.value if f.label == 'VarTable')
		print('Module has {} Variables'.format(len(field_vars.value)))

